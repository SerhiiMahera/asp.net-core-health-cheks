﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Health.Api.Options
{
    public class DatabaseOptions
    {
        public string MySql { get; set; }
    }
}

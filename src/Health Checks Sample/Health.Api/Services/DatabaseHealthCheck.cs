﻿using System;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using Health.Api.Options;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Microsoft.Extensions.Options;
using MySql.Data.MySqlClient;

namespace Health.Api.Services
{
    public class DatabaseHealthCheck : IHealthCheck
    {
        private readonly IOptionsMonitor<DatabaseOptions> _options;
        public DatabaseHealthCheck(IOptionsMonitor<DatabaseOptions> options) => _options = options;
        public Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = new CancellationToken())
        {
            var opt = _options.CurrentValue;
            try
            {
                Console.WriteLine("Connection\n" + opt.MySql);
                Console.WriteLine("");
                Console.WriteLine("");
                using (var connection = new MySqlConnection(opt.MySql))
                {
                    connection.Open();
                }
            }
            catch (Exception)
            {
                return Task.FromResult(HealthCheckResult.Unhealthy("Couldn't open connection to db"));
            }

            return Task.FromResult(HealthCheckResult.Healthy("Checks connection to MsSql database"));
        }
    }
}

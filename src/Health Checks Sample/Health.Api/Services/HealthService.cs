﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Health.Api.Abstractions;
using Microsoft.Extensions.Diagnostics.HealthChecks;

namespace Health.Api.Services
{
    public class HealthService : IHealthService
    {
        private readonly MockHealthCheck _mockHealthCheck;
        public HealthService(MockHealthCheck mockHealthCheck)
        {
            _mockHealthCheck = mockHealthCheck;
        }

        public void ChangeStatus(HealthStatus newStatus)
        {
            _mockHealthCheck.Status = newStatus;
        }       
    }
}

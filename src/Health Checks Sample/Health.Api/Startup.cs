﻿//using Health.Api.Abstractions;

using Health.Api.Abstractions;
using Health.Api.Options;
using Health.Api.Services;
using HealthChecks.UI.Client;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Diagnostics.HealthChecks;

namespace Health.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        //This method gets called by the runtime.Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Register check as transient
            services.AddHealthChecks()
                .AddCheck<DatabaseHealthCheck>("MySqlConnect", failureStatus:HealthStatus.Unhealthy, tags: new[] { "mssql" })
                .AddCheck<MockHealthCheck>("custom", tags: new[] { "custom" })
                .AddCheck<MemoryHealthCheck>("memory", HealthStatus.Degraded, new[] { "memory" });

            services.AddSingleton<IHealthService, HealthService>();
            services.Configure<DatabaseOptions>(Configuration.GetSection("ConnectionStrings"));
            services.Configure<MemoryCheckOptions>("memory", Configuration.GetSection("HealthChecks:Memory"));
            // Change lifetime of health check
            services.AddSingleton<MockHealthCheck>();
            services.AddSingleton<MemoryHealthCheck>();
            services.AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            //Executes all checks and return status
            app.UseHealthChecks("/health", new HealthCheckOptions()
            {
                ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse,
                // Predicate = x => x.Tags.Contains("MsSqlConnect"),
                //Custom response status code
                ResultStatusCodes =
                {
                    [HealthStatus.Degraded] = 200,
                    [HealthStatus.Unhealthy] = 503,
                    [HealthStatus.Healthy] = 200
                },
                AllowCachingResponses = false,
            });

            app.UseHealthChecks("/memory", new HealthCheckOptions()
            {
                //exclude checks only with tag 'memory'
                Predicate = hcr => hcr.Tags.Contains("memory"),
            });

            app.UseMvc();

            app.Run(async x =>
            {
                x.Response.StatusCode = 404;
                await x.Response.WriteAsync("Route not found");
            });
        }
    }
}

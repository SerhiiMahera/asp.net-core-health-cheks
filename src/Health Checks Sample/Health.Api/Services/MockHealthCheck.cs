﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Diagnostics.HealthChecks;

namespace Health.Api.Services
{
    public class MockHealthCheck : IHealthCheck
    {
        public HealthStatus Status { get; set; } = HealthStatus.Healthy;

        public Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context,
            CancellationToken cancellationToken = new CancellationToken())
        {
            return Task.FromResult(new HealthCheckResult(
                Status,
                description: "Mock status sets by developer",
                exception: null));
        }
    }
}



﻿using System;
using System.Data.SqlClient;
using System.Net;
using Health.Api.Abstractions;
using Health.Api.Options;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Microsoft.Extensions.Options;
using MySql.Data.MySqlClient;

namespace Health.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HealthController : ControllerBase
    {
        // Head api/health/{0,1,2}
        [HttpHead("{status}")]
        public IActionResult Post([FromRoute] HealthStatus status, [FromServices]IHealthService service)
        {
            service.ChangeStatus(status);
            return Ok();
        }

        // Get api/health/status
        [HttpGet("status")]
        public IActionResult Get([FromServices]IOptions<DatabaseOptions> dbOptions)
        {
            if (string.IsNullOrWhiteSpace(dbOptions.Value.MySql))
                return StatusCode((int)HttpStatusCode.InternalServerError, "Unhealthy");

            try
            {
                using (var connection = new MySqlConnection(dbOptions.Value.MySql))
                {
                    connection.Open();
                }
            }
            catch (Exception ex)
            {
                return StatusCode((int)HttpStatusCode.ServiceUnavailable, "Unhealthy");
            }

            return new EmptyResult();
        }
    }
}

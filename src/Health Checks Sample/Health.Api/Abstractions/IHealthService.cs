﻿using Microsoft.Extensions.Diagnostics.HealthChecks;

namespace Health.Api.Abstractions
{
    public interface IHealthService
    {
        void ChangeStatus(HealthStatus newStatus);
    }
}
